import { Colors } from './colors';
import { fonts } from './fonts';
import { Platform } from 'react-native';
import { widthPercentageToDP, heightPercentageToDP } from '../componants/React Native Responsive Screen';
import Layout from '../../src/utils/Layout';

export const globalStyles = {

    HeaderAppIcon: {
        height: heightPercentageToDP(14),
        backgroundColor: '#fff',
        borderBottomLeftRadius: 30,
        borderBottomRightRadius: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },

}

