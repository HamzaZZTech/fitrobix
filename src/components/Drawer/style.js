
import React from 'react';
import { StyleSheet, Dimensions, Platform, } from 'react-native';
import { Colors } from '../../styles/colors';
import { fonts } from '../../styles/fonts';
import { widthPercentageToDP, heightPercentageToDP } from '../../components/React Native Responsive Screen'
import Layout from '../../utils/Layout';

import { ifIphoneX } from 'react-native-iphone-x-helper'

export const styles = StyleSheet.create({

    drawerContent: {
        flex: 1,
    },
    userInfoSection: {
        paddingLeft: 20,
    },
    title: {
        fontSize: 16,
        fontFamily: fonts["Gotham-Book"],
        color: Colors.NewBackgroundColor,
        width: widthPercentageToDP(50),
    },
    caption: {
        lineHeight: 18,
        fontSize: 12,
        fontFamily: fonts["Gotham-Medium"],
        color: Colors.NewBackgroundColor,
        width: widthPercentageToDP(50),
        // textTransform: "",
    },
    row: {
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    paragraph: {
        fontWeight: 'bold',
        marginRight: 3,
    },
    drawerSection: {
        // marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 12,
        paddingHorizontal: 16,
    },








    WholeTouch: {
        flexDirection: 'row',
        alignContent: 'center',
        alignItems: 'center',
        // marginLeft: 15
    },
    Icon: {
        height: 30,
        width: 30
    },
    Text: {
        fontFamily: fonts['Gotham-Medium'],
        fontSize: Layout.isSmallDevice ? 12 : 14,
        letterSpacing: .04,
        marginLeft: 15
    },
});

