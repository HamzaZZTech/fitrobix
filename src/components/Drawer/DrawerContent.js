import React, { useState, useEffect, useRef } from 'react'
import { View, TouchableOpacity, Image, Text, Switch } from 'react-native';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';
// import { connect } from 'react-redux';
// import { useSelector, useDispatch } from 'react-redux';
import { Colors } from '../../styles/colors';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { styles } from './style';
import { AuthContext } from '../../utils/Context';
import Layout from '../../utils/Layout';
// import { Container } from 'native-base';

const DrawerContent = (props) => {

    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>

                    {/* HOME */}
                    <TouchableOpacity
                        onPress={() => { props.navigation.navigate('DashBoardJobseeker') }}
                        style={[styles.WholeTouch, { marginLeft: 18, marginTop: Layout.isSmallDevice ? 14 : 2 }]}>

                        <AntDesign
                            name="home"
                            color={Colors.NewBackgroundColor}
                            size={25}
                        />
                        <Text style={styles.Text}>Home</Text>
                    </TouchableOpacity>

                    <View style={{ marginTop: 25 }}></View>


                </View>
            </DrawerContentScrollView>


        </View>
    );
}

export default DrawerContent;
