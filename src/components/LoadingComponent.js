import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ActivityIndicator,
  Image
} from "react-native";
// import { ActivityIndicator } from 'react-native-paper';
import Theme from '../utils/Color'
export default class LoadingComponent extends Component {
  static navigationOptions = {
    header:null
  }
  render() {
    return (
      <View style={[style.container,this.props.style]}>
        <ActivityIndicator color={Theme.primary} size="large"/>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Theme.secondry,
    justifyContent: "center",
    alignItems: "center",
  }
});
