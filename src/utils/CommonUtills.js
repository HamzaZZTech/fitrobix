export function sendNotificationImmediately(message, description, type) {
    showMessage({message: message, description: description, type: type, duration: 3000});
};
