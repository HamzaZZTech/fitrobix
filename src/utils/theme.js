export const Theme = {
    primary: '#045266',
    primaryTrans: '#0B83A0',
    secondry: '#E8ECEF',
    secondryTrans:'#E8ECEF',
    white:'#fff',
    offwhite:'#f4f4f4',
    black:'#000000',
    red:'#ef412f',
    yellow:'#f98e02',
    green:'#28a745',
    blue:'#0288d1',
    transparent:'transparent',
    gray:'#e2e2e2'

};

export default Theme;