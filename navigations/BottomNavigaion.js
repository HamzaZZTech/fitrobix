
import React from 'react'
import {
    StyleSheet,
    LogBox,
  } from 'react-native';
import BottomTabIcons from '../navigations/components/BottomTabIcon';
import BottomTabI from '../navigations/components/BottomTab';
import Theme from '../src/utils/theme';

import screen1 from '../src/screens/screen1';
import screen3 from '../src/screens/screen3';
import screen2 from '../src/screens/screen2';
import screen4 from '../src/screens/screen4';
import {
    getFocusedRouteNameFromRoute,
  } from '@react-navigation/native';
  
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';


export function navigate(name, screen, item) {
    if (isReadyRef.current && navigationRef.current) {
      // Perform navigation if the app has mounted
      navigationRef.current.navigate(name, {
        screen: screen,
        params: {selectedItem: item},
      });
    } else {
      // You can decide what to do if the app hasn't mounted
      // You can ignore this, or add these actions to a queue you can call later
    }
  }
  // enableScreens(true);
  LogBox.ignoreLogs([
    'VirtualizedLists should never be nested',
    'Animated: `useNativeDriver` was not specified',
  ]);
  // LogBox.ignoreAllLogs(true)
  
  
  
  const getTabBarVisibility = route => {
    const routeName = getFocusedRouteNameFromRoute(route);
    if (routeName === 'Setting Screen' || 'Unit Tab' || 'MapScreen') {
      return true;
    }
  
    return false;
  };

const BottomTab = createMaterialBottomTabNavigator();

const BottomTabScreen = () => (
  <BottomTab.Navigator
    barStyle={{
      backgroundColor: Theme.primary,
    }}
    backBehavior={'initialRoute'}
    initialRouteName={'Unit Stack'}
    labeled={false}>
    <BottomTab.Screen
      name={'Unit Stack'}
      component={screen1}
      options={({route}) => ({
        tabBarVisible: getTabBarVisibility(route),
        tabBarIcon: ({focused, horizontal, tintColor}) => {
          return (
            <BottomTabIcons
              focused={focused}
              uri={require('../assets/list.png')}
              title={'Units'}
            />
          );
        },
      })}
    />
    <BottomTab.Screen
      name={'Map Stack'}
      component={screen2}
      options={({route}) => ({
        tabBarVisible: getTabBarVisibility(route),
        tabBarIcon: ({focused, horizontal, tintColor}) => {
          return (
            <BottomTabIcons
              focused={focused}
              uri={require('../assets/monitoring.png')}
              title={'Monitoring'}
            />
          );
        },
      })}
    />

    <BottomTab.Screen
      name={'User Notification'}
      component={screen3}
      options={({route}) => ({
        tabBarVisible: getTabBarVisibility(route),
        tabBarIcon: ({focused, horizontal, tintColor}) => {
          return <BottomTabI focused={focused} title={'Notification'} />;
        },
      })}
    />
    <BottomTab.Screen
      name={'SettingScreen'}
      component={screen4}
      options={({route}) => ({
        tabBarVisible: getTabBarVisibility(route),
        tabBarIcon: ({focused, horizontal, tintColor}) => {
          return (
            <BottomTabIcons
              focused={focused}
              uri={require('../assets/settings.png')}
              title={'Setting'}
            />
          );
        },
      })}
    />
  </BottomTab.Navigator>
);
export default BottomTabScreen;