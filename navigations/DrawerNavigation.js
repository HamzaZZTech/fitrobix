import React from 'react'
import { View,Text } from 'native-base';
import {createStackNavigator} from '@react-navigation/stack';

import BottomTabScreen from '../navigations/BottomNavigaion';

import DrawerContent from '../src/components/Drawer/DrawerContent'

import {createDrawerNavigator} from '@react-navigation/drawer';

import screen1 from '../src/screens/screen1';
import screen3 from '../src/screens/screen3';
import screen2 from '../src/screens/screen2';
import screen4 from '../src/screens/screen4';

const AppStack = createStackNavigator();
const DrawerStack = createDrawerNavigator();
const DrawerStackScreen = () => (
  <DrawerStack.Navigator
    screenOptions={{
      activeTintColor: '#e91e63',
      itemStyle: {marginVertical: 5},
    }}
    drawerContent={(props) => <DrawerContent {...props} />}
    >
    <DrawerStack.Screen name="AppStack" component={AppStackScreen} />
  </DrawerStack.Navigator>
);
    
const AppStackScreen = () => (
  <AppStack.Navigator>
    <AppStack.Screen
      name="DashBoardJobseeker"
      component={BottomTabScreen}
      options={{
        headerShown: false,
      }}
    />

    <AppStack.Screen
      name="Job Listing"
      component={screen2}
      options={{
        headerShown: false,
      }}
    />

    <AppStack.Screen
      name="ApplyJob"
      component={screen2}
      options={{
        headerShown: false,
      }}
    />

    <AppStack.Screen
      name="TrendingJob"
      component={screen2}
      options={{
        headerShown: false,
      }}
    />
  </AppStack.Navigator>
);
export default DrawerStackScreen;