import React from 'react';
import { View, Image, Text } from 'react-native';
import Theme from '../../src/utils/theme';
const TabBarIcon = ({ focused, uri, title }) => {
    return (
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: focused ? Theme.primaryTrans : Theme.primary }}>
            <Image
                style={{ width: 25, height: 25 }}
                source={uri}
            />
            <Text style={{ marginTop: 5, fontWeight: 'bold', fontSize: 11, color: Theme.white }}>{title}</Text>
        </View>
    );
}

export default TabBarIcon;