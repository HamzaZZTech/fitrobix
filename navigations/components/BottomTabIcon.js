import React from 'react';
import {View, Image, Text, Platform} from 'react-native';
import Theme from '../../src/utils/theme';
import Layout from '../../src/utils/Layout';
class BottomTabIcon extends React.Component {

    render() {
        return (
            <View style={{}}>
                <Image
                    style={{
                    width: 25,
                    height: 25
                }}
                    source={this.props.uri}/> 
                    {this.props.focused && <View
                    style={{
                    width: 8,
                    height: 8,
                    borderRadius: 8 / 2,
                    backgroundColor: Theme.white,
                    marginTop: 4,
                    alignSelf: 'center'
                }}/>}
            </View>
        );
    }
}
export default BottomTabIcon;