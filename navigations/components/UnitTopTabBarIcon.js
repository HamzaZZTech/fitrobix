import React from 'react';
import {View, Image, Text} from 'react-native';
import Theme from '../../src/utils/theme';
import Layout from '../../src/utils/Layout';
const UnitTopTabBarIcon = ({focused, title}) => {

    return (
        <View
                style={{
                backgroundColor: focused
                    ? Theme.primary
                    : Theme.primaryTrans,
                    borderWidth:1,
                    borderColor:Theme.primary,
                width: ((Layout.window.width / 2) - 20),
                justifyContent: 'center',
                borderRadius: 5
            }}>
                <Text
                    style={{
                    color: focused
                        ? Theme.white
                        : Theme.white,
                    margin: 8,
                    fontSize: 14,
                    fontWeight: 'bold',
                    textAlign: 'center'
                }}>
                    {title}
                </Text>
            </View>
    );
}

export default UnitTopTabBarIcon;