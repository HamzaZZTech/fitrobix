import React from 'react';
import {View, Image, Text, Platform} from 'react-native';
import Theme from '../../src/utils/theme';
import Layout from '../../src/utils/Layout';
import Entypo from 'react-native-vector-icons/Entypo'
class BottomTab extends React.Component {

    render() {
        return (
            <View style={{}}>
                <Entypo name={"notification"} 
                size={25}
                color={Theme.white}/> 
                {this.props.focused && <View
                    style={{
                    width: 8,
                    height: 8,
                    borderRadius: 8 / 2,
                    backgroundColor: Theme.white,
                    marginTop: 4,
                    alignSelf: 'center'
                }}/>}
            </View>
        );
    }
}
export default BottomTab;