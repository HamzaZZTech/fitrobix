/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  LogBox,
} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import DrawerStackScreen from './navigations/DrawerNavigation';
export const isReadyRef = React.createRef();
export const navigationRef = React.createRef();

export default App = () => {
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}>
      <DrawerStackScreen />
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({});
